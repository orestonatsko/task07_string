package task_stringutils;

import java.util.Arrays;

public class StuffUtils {
    private String[] stuffs;
    private int count;

    public StuffUtils() {
        stuffs = new String[count];
    }

    public StuffUtils(Stuff... stuffs) {
        if (stuffs == null) {
            throw new NullPointerException();
        }
        this.stuffs = new String[stuffs.length];
        for (Stuff s : stuffs) {
            this.stuffs[count++] = s.getName();
        }
    }

    public int length() {
        return count;
    }

    public StuffUtils append(Stuff s) {
        if (s == null) {
            return this;
        }
        String name = s.getName();
        append(name);
        return this;
    }

    private void append(String name) {
        String[] oldStaff = stuffs;
        stuffs = Arrays.copyOf(oldStaff, ++count);
        stuffs[count -1] = name;
    }

    @Override
    public String toString() {
        return String.join(", ", this.stuffs);
    }
}
