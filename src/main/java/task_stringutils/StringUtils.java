package task_stringutils;

import java.util.Arrays;

public class StringUtils {
    private char[] value;
    private int count;

    public StringUtils() {
        count = 0;
    }

    public StringUtils(String v) {
        value = v.toCharArray();
        count = v.length();
    }


    public StringUtils append(String str) {
        if (str == null) {
            return this;
        }
        int len = str.length();
        value = Arrays.copyOf(value, count + len);
        str.getChars(0, len, value, count);
        count += len;
        return this;
    }

    public int length() {
        return count;
    }

    @Override
    public String toString() {
        return new String(value, 0, count);
    }
}
